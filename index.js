const express = require('express');
const app = express();
const port = 80;
app.get('/calc', (req, res) => {
  const { param1, param2 } = req.query; // Use param1, param2 to match the query parameter keys
  console.log(param1);
  console.log('Narendra');
  console.log(param2);
  res.send('Answer is ' + (param1 * param2)); // Ensure the variables match for the calculation
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
